FROM openjdk:12
ADD target/docker01-springboot.jar docker01-springboot.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker01-springboot.jar"]