package docker01springboot.docker01springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Docker01SpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Docker01SpringbootApplication.class, args);
	}

}
